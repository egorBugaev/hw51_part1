import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Movie from './App';
import registerServiceWorker from './registerServiceWorker';

const data = {
    movie1: {
        title: 'The Rock',
        genre: 'Action, Adventure, Thriller',
        year: '1996',
        poster:'https://images-na.ssl-images-amazon.com/images/M/MV5BZDJjOTE0N2EtMmRlZS00NzU0LWE0ZWQtM2Q3MWMxNjcwZjBhXkEyXkFqcGdeQXVyNDk3NzU2MTQ@._V1_SX300.jpg'

    },
    movie2: {
        title: 'Moana',
        genre: 'Animation, Adventure',
        year: '2016',
        poster:'https://images-na.ssl-images-amazon.com/images/M/MV5BMjI4MzU5NTExNF5BMl5BanBnXkFtZTgwNzY1MTEwMDI@._V1_SX300.jpg'

    },
    movie3: {
        title: 'Armageddon',
        genre: 'Action, Adventure, Sci-Fi',
        year: '1998',
        poster:'https://images-na.ssl-images-amazon.com/images/M/MV5BMGM0NzE2YjgtZGQ4YS00MmY3LTg4ZDMtYjUwNTNiNTJhNTQ5XkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_SX300.jpg'

    },

};

ReactDOM.render(<Movie movie1={data.movie1} movie2={data.movie2} movie3={data.movie3} />, document.getElementById('root'));
registerServiceWorker();
