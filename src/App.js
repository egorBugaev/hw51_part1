import React, { Component } from 'react';
import './App.css';

function Mark(props) {
    console.log(props, 'PROPS');
    return(
        <div className="movie">
            <h1>{props.title}</h1>
            <p>Genre: {props.genre}</p>
            <p>Year: {props.year}</p>
            <img src={props.poster} alt='alt'/>
        </div>

    );
}
class Movie extends Component {
 render(){
  return(  <div className="movie-card">
        <Mark title={this.props.movie1.title}
                genre={this.props.movie1.genre}
                year={this.props.movie1.year}
                poster={this.props.movie1.poster}/>
          <Mark title={this.props.movie2.title}
                genre={this.props.movie2.genre}
                year={this.props.movie2.year}
                poster={this.props.movie2.poster}/>
          <Mark title={this.props.movie3.title}
                genre={this.props.movie3.genre}
                year={this.props.movie3.year}
                poster={this.props.movie3.poster}/>

    </div>
  )
}
}



export default Movie;
